<?php

namespace App\DataFixtures;

use Faker\Factory;
use App\Entity\Mark;
use App\Entity\User;
use App\Entity\Recipe;
use App\Entity\Ingredient;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create();

        $users = [];
        for ($i=0; $i < 10; $i++) { 
            $user = new User();
            $user->setFirstName($faker->firstName())
                ->setLastName($faker->lastName())
                ->setEmail($faker->email())
                ->setRoles(['ROLE_USER'])
                ->setPassword($this->encoder->encodePassword($user, 'password'));

            $ingredients = [];
            for ($j=0; $j < 15; $j++) { 
                $ingredient = new Ingredient();
                $ingredient->setName($faker->word())
                    ->setprice(mt_rand(10, 500) / 10)
                    ->setUser($user);

                $manager->persist($ingredient);
                $ingredients[] = $ingredient;
            }

            $recipes = [];
            for ($k=0; $k < 10; $k++) { 
                $recipe = new Recipe();
                $recipe->setName($faker->word())
                    ->setTime(mt_rand(10, 60))
                    ->setNbPeople(mt_rand(1, 5))
                    ->setDifficulty(mt_rand(0, 5))
                    ->setDetails($faker->text())
                    ->setprice(mt_rand(100, 1000) /10)
                    ->setIsFavorite(mt_rand(0, 1) === 1 ? true : false)
                    ->setUser($user);

                foreach ($ingredients as $ingredient) {
                    if(mt_rand(0, 1) === 1) {
                        $recipe->addIngredient($ingredient);
                    }
                }

                $manager->persist($recipe);
                $recipes[] = $recipe;
            }

            $manager->persist($user);
            $users[] = $user;
        }

        $marks = [];
        foreach ($users as $user) {
            foreach ($recipes as $recipe) {
                if($recipe->getUser() !== $user) {
                    if(mt_rand(0, 1) === 1) {
                        $mark = new Mark();
                        $mark->setMark(mt_rand(0, 10))
                            ->setUser($user)
                            ->setRecipe($recipe);

                        $manager->persist($mark);
                        $marks[] = $mark;
                    }       
                }
            }
        }

        $manager->flush();
    }
}
