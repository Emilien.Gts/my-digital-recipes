<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\RecipeRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use Doctrine\Common\Collections\Collection;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=RecipeRepository::class)
 * @ApiResource(
 *  collectionOperations={"GET", "POST"},
 *  itemOperations={"GET", "DELETE", "PUT", "PATCH"},
 *  normalizationContext={
 *      "groups"={"recipes_read"}
 *  }
 * )
 * 
 * @ApiFilter(
 *  SearchFilter::class, 
 *  properties={
 *      "name": "partial", 
 *      "time": "exact", 
 *      "nbPeople": "exact",
 *      "difficulty": "exact",
 *      "price": "exact",
 *      "isFavorite": "exact",
 *      "createdAt": "exact",
 *      "updatedAt": "exact"
 *  }
 * )
 * 
 * @ApiFilter(
 *  OrderFilter::class, 
 *  properties={"id","time","nbPeople","difficulty","price","isFavorite","createdAt","updatedAt"}
 * )
 */
class Recipe
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups({"recipes_read", "users_read"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotBlank
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "Le nom doit être au moins de {{ limit }} caractères",
     *      maxMessage = "Le nom doit être au plus de  {{ limit }} caractères",
     *      allowEmptyString = false
     * )
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotBlank
     * @Assert\Positive
     */
    private $time;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotBlank
     * @Assert\Positive
     */
    private $nbPeople;

    /**
     * @ORM\Column(type="integer")
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotBlank
     * @Assert\Positive
     */
    private $difficulty;

    /**
     * @ORM\Column(type="text")
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotNull
     */
    private $details;

    /**
     * @ORM\Column(type="float")
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotBlank
     * @Assert\Positive
     */
    private $price;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotBlank
     */
    private $isFavorite;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotBlank
     */
    private $createdAt;

    /**
     * @ORM\Column(type="datetime")
     * @Groups({"recipes_read", "users_read", "ingredients_read"})
     * 
     * @Assert\NotBlank
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="recipes")
     * @ORM\JoinColumn(nullable=false)
     * @Groups({"recipes_read"})
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity=Ingredient::class, inversedBy="recipes")
     * @Groups({"recipes_read"})
     */
    private $ingredients;

    /**
     * @ORM\OneToMany(targetEntity=Mark::class, mappedBy="recipe")
     * @Groups({"recipes_read"})
     */
    private $marks;

    public function __construct()
    {
        $this->setUpdatedAt(new \DateTime('now'));    
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
        $this->ingredients = new ArrayCollection();
        $this->marks = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTime(): ?int
    {
        return $this->time;
    }

    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getNbPeople(): ?int
    {
        return $this->nbPeople;
    }

    public function setNbPeople(int $nbPeople): self
    {
        $this->nbPeople = $nbPeople;

        return $this;
    }

    public function getDifficulty(): ?int
    {
        return $this->difficulty;
    }

    public function setDifficulty(int $difficulty): self
    {
        $this->difficulty = $difficulty;

        return $this;
    }

    public function getDetails(): ?string
    {
        return $this->details;
    }

    public function setDetails(string $details): self
    {
        $this->details = $details;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getIsFavorite(): ?bool
    {
        return $this->isFavorite;
    }

    public function setIsFavorite(bool $isFavorite): self
    {
        $this->isFavorite = $isFavorite;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return Collection|Ingredient[]
     */
    public function getIngredients(): Collection
    {
        return $this->ingredients;
    }

    public function addIngredient(Ingredient $ingredient): self
    {
        if (!$this->ingredients->contains($ingredient)) {
            $this->ingredients[] = $ingredient;
        }

        return $this;
    }

    public function removeIngredient(Ingredient $ingredient): self
    {
        $this->ingredients->removeElement($ingredient);

        return $this;
    }

    /**
     * @return Collection|Mark[]
     */
    public function getMarks(): Collection
    {
        return $this->marks;
    }

    public function addMark(Mark $mark): self
    {
        if (!$this->marks->contains($mark)) {
            $this->marks[] = $mark;
            $mark->setRecipe($this);
        }

        return $this;
    }

    public function removeMark(Mark $mark): self
    {
        if ($this->marks->removeElement($mark)) {
            // set the owning side to null (unless already changed)
            if ($mark->getRecipe() === $this) {
                $mark->setRecipe(null);
            }
        }

        return $this;
    }
}
