<?php

namespace App\Events;

use App\Entity\Recipe;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class RecipeUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserForRecipe', EventPriorities::PRE_VALIDATE]
        ];
    }
    
    public function setUserForRecipe(ViewEvent $event)
    {
        $recipe = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if($recipe instanceof Recipe && $method === 'POST') {
            $user = $this->security->getUser();
            $recipe->setUser($user);
        }
    }
}