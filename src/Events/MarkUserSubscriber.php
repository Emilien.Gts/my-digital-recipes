<?php

namespace App\Events;

use App\Entity\Mark;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use ApiPlatform\Core\EventListener\EventPriorities;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class MarkUserSubscriber implements EventSubscriberInterface
{
    /**
     * @var Security
     */
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => ['setUserForMark', EventPriorities::PRE_VALIDATE]
        ];
    }
    
    public function setUserForMark(ViewEvent $event)
    {
        $mark = $event->getControllerResult();
        $method = $event->getRequest()->getMethod();

        if($mark instanceof Mark && $method === 'POST') {
            $user = $this->security->getUser();
            $mark->setUser($user);
        }
    }
}