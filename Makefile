COMPOSER 	= @composer
PHP 			= @php
SYMFONY 	= $(PHP) bin/console


## —— 🔥 App —————————————————————————————————————————————————————————————————
init: ## Init the project
	$(MAKE) install-vendor
	$(MAKE) set-db
	
start: ## Start the app
	$(PHP) -S localhost:8001 -t public/

update: ## Update the app
	$(MAKE) update-vendor

cc: ## Clear the app's cache
	$(MAKE) cc-symfony
	$(MAKE) composer-cc


## —— 🎻 Composer —————————————————————————————————————————————————————————————
install-vendor: ## Installs all vendor dependencies
	$(COMPOSER) install --prefer-dist

update-vendor: ## Updates all vendor dependencies
	$(COMPOSER) update
	
composer-cc: ## Clear the composer's cache
	$(COMPOSER) cc


## —— 🎶 Symfony ——————————————————————————————————————————————————————————————
set-db: ## Set database
	$(SYMFONY) d:d:d --if-exists --force
	$(SYMFONY) d:d:c
	$(SYMFONY) d:m:m --no-interaction
	$(SYMFONY) d:f:l --no-interaction

clean-db: ## Reinit database
	$(MAKE) set-db
	$(SYMFONY) d:m:m --no-interaction
	$(SYMFONY) d:f:l --no-interaction

cc-symfony:	## Clear cache
	$(SYMFONY) c:c


## —— 🛠️  Others ————————————————————————————————————————————————————————————————
help: ## List of commands
	@grep -E '(^[a-zA-Z0-9_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'
