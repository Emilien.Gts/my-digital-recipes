<!-- NAME -->

## Name

MyDigitalRecipes API

<!-- DESCRIPTION -->

## Description

The principle of MyDigitalRecipes is to create ingredients / recipes, in order to list and manage your recipes. This project is based on the PHP Framework [Symfony](https://symfony.com/) as well as on the [API Platform](https://api-platform.com/) library, which is a library that allows to make REST and GraphQL APIs.

<!-- CONTEXT -->

## Context

The idea for this project was taken from a practical work carried out during my third year. This idea was then taken up again during a Flutter TP during my first year of Master. For this TP, it was therefore necessary to set up an API.

<!-- INSTALLATIOn -->

## Installation

The first stage of the installation will be to install all the outbuildings using [Composer](https://getcomposer.org/)

    composer install

You must then set up an environment for the database and adapt the environment variable `DATABASE_URL`. You can then change the environment variable `DATABASE_URL` at your convenience. For the example, the username is `root`, the password is `toor` and the database name is `mydigitalrecipes`.

    DATABASE_URL=mysql://root:toor@127.0.0.1:3306/mydigitalrecipes?serverVersion=5.7

<!-- USAGE -->

## Usage

Pour utiliser l'application, vous avez simplement besoin de lancer la commande suivante au sein de votre terminal

    symfony serve

To do this, you need to install the [Symfony CLI](https://symfony.com/download)

<!-- LICENSE -->

## License

[License](https://gitlab.com/Emilien.Gts/my-digital-recipes/-/blob/master/LICENSE)

<!-- PROJECT STATUS -->

## Project Status

This project is currently on pause. Some features will be added later to make the project more complete.
